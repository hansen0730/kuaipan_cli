#!/usr/bin/env python
# coding=utf-8
"""
Command Line Client For Kuaipan 

author: ksc (http://blog.geekli.cn)
api   : http://www.kuaipan.cn/developers/document.htm
"""
from __future__ import print_function

import time
import os,sys

from kplib.session import KuaipanSession
from kplib.session import log
from kplib.KuaipanAPI import KuaipanAPI
from kplib.KuaipanCLI import KuaipanCLI
from kplib.kpArgparse import kpParser

import ConfigParser
import logging 


__version='0.8.4'
__usage='''%prog [--version] [--help] <command> [<args>]'''
script_path=sys.argv[0]

parser=kpParser(__version)

if len(sys.argv) ==1 :
    parser.parse_args(['-h'])
    sys.exit()
    
args = parser.parse_args()

config_file=args.config_file

if os.path.isabs(config_file)==False:
    _config_file=os.getcwd()+os.sep+config_file
    if not os.path.isfile(_config_file):
        _config_file=os.path.dirname(script_path)+os.sep+config_file
    config_file=os.path.realpath(_config_file)

if os.path.isfile(config_file)==False:
    print('config file does not exist')
    sys.exit()


 
cf = ConfigParser.ConfigParser({'root':'app_folder','log_level':'info'})
conf={}
try:
    cf.read(config_file)
    consumer_key        = cf.get("def", "consumer_key")
    consumer_secret     = cf.get("def", "consumer_secret")
    access_type=cf.get('def', 'root')
    log_level=cf.get('def', 'log_level')
        
except Exception as e:
    print('config: ',config_file)
    print('Parse config file failed')
    print(e)
    sys.exit()

#set logging    
try:
    loglevel=getattr(logging,log_level.upper())
except Exception as e:
    sys.exit( 'log_level must be one of NOTSET,DEBUG,INFO,WARNING,ERROR,CRITICAL  ')

logfile=os.path.join(os.path.dirname(script_path),'log.txt')

logging.basicConfig(filename =logfile, level =loglevel,
                filemode = 'a', format = '%(asctime)s - %(levelname)s: %(message)s')    

logging.debug( config_file)
    
logging.debug( 'consumer_key \t'+consumer_key)
#logging.debug( 'consumer_secret:\t'+consumer_secret)


def _set_oauth():
    '''获取用户授权'''
    global cf,config_file,sess
    _oauth=sess.get_oauth_token()
    print('oauth success')
    logging.debug(_oauth)
    if not cf.has_section("oauth"):
        cf.add_section('oauth')
        
    cf.set('oauth','user_id',_oauth['user_id'])
    cf.set('oauth','oauth_token',_oauth['oauth_token'])
    cf.set('oauth','oauth_token_secret',_oauth['oauth_token_secret'])
    cf.set('oauth','expires_in',_oauth['expires_in'])
    cf.set('oauth','charged_dir',_oauth['charged_dir'])
    cf.write(open(config_file,'w'))
    
sess = KuaipanSession(consumer_key, consumer_secret,access_type)
sess.debug=False

if not cf.has_section("oauth"):
    print( 'Requires the user to authorize')
    _set_oauth()
    sys.exit()
        


logging.info(args)
#子命令
action= args.subcommand


#获取用户授权
if action=='oauth':
    _set_oauth()    
    sys.exit()

conf['oauth_token']         = cf.get("oauth", "oauth_token")
conf['oauth_token_secret']  = cf.get("oauth", "oauth_token_secret")
sess.set_oauth_token(conf['oauth_token'],conf['oauth_token_secret'])

    
api = KuaipanAPI(sess)
cli = KuaipanCLI(api)


if action in ['account_info','info']:
    cli.info()
    
elif action == 'mkdir':
    cli.mkdir(args.path)
    
elif action in ['metadata','ls']:
    cli.ls(args.path)
    
elif action in ['rm','delete']:
    cli.rm(args.path)
    
elif action in ['mv','move']:
    cli.mv(args.src, args.dst)

elif action in ['cp','copy']:
    cli.cp(args.src, args.dst)
    
elif action in ['put','upload']:
    os.path.isfile(args.src)
    cli.put(args.src, args.dst)

elif action in ['get','download']:
    cli.get(args.src, args.dst)
  
elif action in ['shares']:
    cli.shares(args.path, args.access_code)
   
else:
    print('wrong subcommand')
    print(args)
