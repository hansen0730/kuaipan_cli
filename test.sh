#单元测试
./kuaipan.py -v
./kuaipan.py info
./kuaipan.py rm test/

#创建目录
./kuaipan.py mkdir test/
#递归创建目录
./kuaipan.py mkdir test/a/b/c



./kuaipan.py ls test/a/b/c

#上传
./kuaipan.py put README.md test/readme.txt
./kuaipan.py ls test/readme.txt
#自动补全名字
./kuaipan.py put log.txt test/
./kuaipan.py ls test/log.txt

#查看目录
./kuaipan.py ls test

#复制
./kuaipan.py cp test/log.txt test/a/log.txt
./kuaipan.py ls test/a/log.txt
./kuaipan.py cp test/log.txt test/a/b/c/log.txt


#剪切
./kuaipan.py mv test/log.txt test/log2.txt
./kuaipan.py ls test/log2.txt

#共享
./kuaipan.py shares test/readme.txt

#下载
./kuaipan.py get test/a/log.txt temp/log.txt
#下目录
./kuaipan.py get test/ temp/

#上传目录
./kuaipan.py put  temp/ test/v2/