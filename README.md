#Kuaipan Cli Client （目前为测试版本）

快盘在命令行下的客户端，支持linux、window，主要应用在linux服务器下
####使用方法
首次使用请先新建配置文件 kp.ini 内容如下

    [def]
    consumer_key = 填写你申请的consumer_key
    consumer_secret = 填写你申请的consumer_secret
    root = kuaipan 或者 app_folder
然后获取授权

    kuaipan.py oauth
    
若不配置文件的使用相对路径（默认配置文件名称kp.ini），程序会优先匹配相对当前工作目录的文件,若不存在则匹配相对程序的配置文件
####依赖 
>* python 2.7 
>* python库 poster

安装方法（debian、ubuntu）

     apt-get install python-setuptools
     easy_install poster

编译版本[下载地址](http://geekli.cn/downloads/)

####使用说明
    usage: kuaipan.py [-h] [-v] [-c FILE]
                      {oauth,info,ls,rm,mkdir,mv,cp,put,get,shares} ...

    optional arguments:
      -h, --help            show this help message and exit
      -v, --version         show program's version number and exit
      -c FILE, --config FILE
                            the path of the configuration file

    subcommands:
      valid subcommands

      {oauth,info,ls,rm,mkdir,mv,cp,put,get,shares}
        oauth               get oauth_token
        info                show user account infomation
        ls                  list contents of remote directory
        rm                  remove files or directories
        mkdir               mkdir directorie
        mv                  move (rename) files
        cp                  copy files and directories
        put                 put one file
        get                 receive file
**常用命令**

    #查看基本信息
    $ kuaipan info
    ksc@kuaipan.cn 94.25M/5.0G 0.02%used maxfilesize:2.0G
    
    #上传文件
    $ kuaipan put img.zip test/img2.zip    
    
    #下载文件
    $ kuaipan get /test/img2.zip /tmp/
    [==================================================] 100%
    download success
    save to /tmp/img2.zip
    
    #下载目录
    $ kuaipan get test/v2/ /temp/v5/
    [==================================================] 100%
    download success
    save to /temp/v5/a/b/c/log4.txt
    [==================================================] 100%
    download success
    save to /temp/v5/b.txt

    
####计划
* <del>支持目录下载</del>
* <del>支持目录上传</del>