# coding=utf-8

import argparse
import sys

def kpParser(version=None):
    # create the base parser with a subparsers argument
    parser = argparse.ArgumentParser(argument_default='-h')
    parser.add_argument('-c','--config',default='kp.ini',dest='config_file', metavar="FILE", help='the path of the configuration file')
    parser.add_argument('-v','--version', action='version', version=version, help="show program's version number and exit")

    subparsers = parser.add_subparsers(title='subcommands', description='valid subcommands',dest='subcommand')

    sparser = subparsers.add_parser('oauth',help='get oauth_token')
    
    sparser = subparsers.add_parser('info',help='show user account infomation')

    sparser = subparsers.add_parser('ls',help='list contents of remote directory')
    sparser.add_argument('path',default='/', nargs='?')

    sparser = subparsers.add_parser('rm',help='remove files or directories')
    sparser.add_argument('path', help='')
    
    sparser = subparsers.add_parser('mkdir',help='mkdir directorie')
    sparser.add_argument('path', help='')

    sparser = subparsers.add_parser('mv',help='move (rename) files')
    sparser.add_argument('src', help='')
    sparser.add_argument('dst')

    
    sparser = subparsers.add_parser('cp',help='copy files and directories')
    sparser.add_argument('src', help='')
    sparser.add_argument('dst')
    
    sparser = subparsers.add_parser('put',help='put one file')
    sparser.add_argument('src', help='local path')
    sparser.add_argument('dst', default=None, nargs='?',help='remote path')

    sparser = subparsers.add_parser('get',help='receive file')
    sparser.add_argument('src', help='remote path')
    sparser.add_argument('dst', default=None, nargs='?', help='local path')

    sparser = subparsers.add_parser('shares',help='share a file')
    sparser.add_argument('path', help='')
    sparser.add_argument('access_code', default='', nargs='?', help='Access code [a-zA-Z]，6-10位。')

    return parser


if __name__ =='__main__':
    print(kpParser().parse_args())
